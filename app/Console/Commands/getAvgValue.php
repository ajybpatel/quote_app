<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use \App\State;

class getAvgValue extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'getAvgValue';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Average prices';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $states = State::all();

        foreach ($states as $state) {
            $averages = $state->getAverage();
            $this->output->write($state->state . " >-> ");
            foreach ($averages as $average) {
                foreach ($average as $key => $value) {
                    if($value != null){
                        $this->output->write($key . '(' . $value . '),  ');
                    }
                }
                $this->output->write(PHP_EOL);
            }
            $this->output->write(PHP_EOL);
        }
    }
}
