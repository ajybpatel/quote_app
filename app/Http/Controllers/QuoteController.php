<?php

namespace App\Http\Controllers;

use App\Quote;
use Illuminate\Http\Request;
use Redirect;

class QuoteController extends Controller
{
    //
     
    public function index()
    {
        $data['quote'] = Quote::all();
   
        return view('quote.list',$data);
    }

    public function show($id)
    {
    }

    public function create()
    {
        return view('quote.create');
    }

    public function store(Request $request)
    {
        $request->validate([
            'tvshow' => 'required',
            'season' => 'required',
            'content' => 'required',
        ]);
   
        Quote::create($request->all());
    
        return Redirect::to('quote')
       ->with('success','Great! Quote created successfully.');
    }
    public function edit($id)
    {   
        $where = array('id' => $id);
        $data['quote_info'] = Quote::where($where)->first();
 
        return view('quote.edit', $data);
    }

    public function update(Request $request, $id)
    {
        $request->validate([
            'tvshow' => 'required',
            'season' => 'required',
            'content' => 'required',
        ]);
         
        $update = ['tvshow' => $request->tvshow, 'season' => $request->season, 'content' => $request->content];
        Quote::where('id',$id)->update($update);
   
        return Redirect::to('quote')
       ->with('success','Great! Quote updated successfully');
    }

    public function destroy($id)
    {
        Quote::where('id',$id)->delete();
   
        return Redirect::to('quote')->with('success','Quote deleted successfully');
    }
    
}
