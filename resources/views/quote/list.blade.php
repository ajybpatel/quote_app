@extends('quote.layout')
 
@section('content')
<div class="row mt40">
   <div class="col-md-10">
    <h2>Quotes</h2>
   </div>
   <div class="col-md-2">
    <a href="{{ route('quote.create') }}" class="btn btn-danger">Add Quote</a>
   </div>
   <br><br>
    @if ($message = Session::get('success'))
        <div class="alert alert-success">
            <p>{{ $message }}</p>
        </div>
    @endif
    @if ($errors->any())
    <div class="alert alert-danger">
        <strong>Opps!</strong> Something went wrong<br><br>
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
   @endif
  
    
</div>

<div class="row">
@foreach($quote as $note)
      <div class="col-md-6">
         <div class="card flex-md-row mb-4 shadow-sm h-md-250">
            <div class="card-body d-flex flex-column align-items-start">
               <strong class="d-inline-block mb-2 text-success">{{ $note->tvshow }}</strong>
               <h6 class="mb-0">
                  <a class="text-dark" href="#">{{ $note->season }}</a>
               </h6>
               <div class="mb-1 text-muted small">{{ date('d m Y', strtotime($note->created_at)) }}</div>
               <p class="card-text mb-auto">{{ $note->content }}</p>
         
               <form action="{{ route('quote.destroy', $note->id)}}" method="post">
                  {{ csrf_field() }}
                  @method('DELETE')
                  <button class="btn btn-outline-danger btn-sm" type="submit">Delete</button>
                </form>
                <a class="btn btn-outline-success btn-sm" href="{{ route('quote.edit',$note->id)}}">Re-write</a>
            </div>
            <img class="card-img-right flex-auto d-none d-lg-block" alt="Thumbnail [200x250]" src="https://picsum.photos/seed/picsum/200/300" style="width: 200px; height: 250px;">
         </div>
      </div>
      @endforeach
   </div>

@endsection
</div>