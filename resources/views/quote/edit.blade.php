@extends('quote.layout')
   
@section('content')
 
<div class="row">
    <div class="col-lg-12 mt40">
        <div class="pull-left">
            <h2>Update Quote</h2>
        </div>
    </div>
</div>
    
@if ($errors->any())
    <div class="alert alert-danger">
        <strong>Opps!</strong> Something went wrong<br>
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif
    
<form action="{{ route('quote.update', $quote_info->id) }}" method="POST" name="update_note">
    {{ csrf_field() }}
    @method('PATCH')
   
     <div class="row">
        <div class="col-md-12">
            <div class="form-group">
                <strong>TV Show</strong>
                <input type="text" name="tvshow" class="form-control" placeholder="Enter TV show" value="{{ $quote_info->tvshow }}">
            </div>
        </div>
        <div class="col-md-12">
            <div class="form-group">
                <strong>Season</strong>
                <input type="text" name="season" class="form-control" placeholder="Enter Season" value="{{ $quote_info->season }}">
            </div>
        </div>
        <div class="col-md-12">
            <div class="form-group">
                <strong>Content</strong>
                <textarea class="form-control" col="4" name="content" placeholder="Enter Description" >{{ $quote_info->content }}</textarea>
            </div>
        </div>
        <div class="col-md-12">
                <button type="submit" class="btn btn-primary">Submit</button>
        </div>
    </div>
    
</form>
@endsection